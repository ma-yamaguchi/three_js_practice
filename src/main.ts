import * as THREE from "three-full";

window.addEventListener('DOMContentLoaded', () => {
// 幅、高さ取得
    const width  = window.innerWidth;
    const height = window.innerHeight;

// レンダラの作成、DOMに追加
    const renderer = new THREE.WebGLRenderer();
    renderer.setSize(width, height);
    renderer.setClearColor(0xf3f3f3, 1.0);
    document.body.appendChild(renderer.domElement);

// シーンの作成、カメラの作成と追加、ライトの作成と追加
    const scene  = new THREE.Scene();
    const camera = new THREE.PerspectiveCamera(50, width / height, 1, 100 );
    camera.position.set(0, 1, 5);
    const light  = new THREE.AmbientLight(0xffffff, 1);
    scene.add(light);

    const grid   = new THREE.GridHelper(10, 5);
    scene.add(grid);

    const loader = new THREE.GLTFLoader();
    const url = 'assets/B.glb';
    let obj_b;

    loader.load(url, (data) => {
        const glb = data;
        obj_b = data.scene;
        obj_b.scale.set(8,8,8);
        scene.add(obj_b);

    });

    const tick = () => {
        requestAnimationFrame(tick);
        if(obj_b != undefined) {
            obj_b.rotation.y += 0.05;
        }

        // 描画
        renderer.render(scene, camera);
    };
    tick();

});